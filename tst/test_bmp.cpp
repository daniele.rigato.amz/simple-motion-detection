#include<cstdio>

#include "bmp.h"
using namespace std;

#include "testlib.h"

void testLoad() {
  BmpImg<unsigned char> img;
  img.load("data/grey.bmp");
}

void testLoadSave() {
  BmpImg<unsigned char> img;
  img.load("data/grey.bmp");
  img.save("data/grey.copy.bmp");
}

void testLoadChangeSaveReload() {
  BmpImg<unsigned char> img, img1;
  img.load("data/grey.bmp");
  MATRIX_EL(img.getPixels(), 100, 200) = 234; 
  img.save("data/grey.alt.bmp");
  img1.load("data/grey.alt.bmp");
  TS_assert(EQe(MATRIX_EL(img1.getPixels(), 100, 200), 234, 0),
	    "the change did not happen");
}

int main() {

  TS_addTest(testLoad, "testLoad");
  TS_addTest(testLoadSave, "testLoadSave");
  TS_addTest(testLoadChangeSaveReload, "testLoadChangeSaveReload");
  //TS_addTest(, "");
  
  return TS_runTests("bmp");
}
