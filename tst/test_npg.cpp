
#include "npg.h"

#include<iostream>
#include "testlib.h"

using namespace std;

#define MAX_ITERATIONS 1000
#define CONVERGENCE_STABILITY_IT 100

NPGTrainingConfig npgConfig = {5, 0.97, -0.13, 0.1, 0.7};
float inSamples[MAX_ITERATIONS];

void trainNpgAndCheckStability(float target, float eps, int outputDbg=0) {
  int i=0, stableIt=0;
  NPG<float, 3> npg;
  for (i=0; i<MAX_ITERATIONS && stableIt<CONVERGENCE_STABILITY_IT; i++) {
    npg.train(inSamples[i], npgConfig);
    stableIt = (npg.isStable(npgConfig))?stableIt+1:0;
    if(outputDbg) {npg.print();}
  }
  int reachedStability = i - CONVERGENCE_STABILITY_IT;
  npg.print();
  TS_debug("npg trained in %d iteration (max it=%d)",
	   reachedStability,
	   MAX_ITERATIONS-CONVERGENCE_STABILITY_IT);
  TS_debug("strongest value %f, weight %f",
	     npg.getStrongestValue(), npg.getStrongestW());  
  if(TS_assert(i<MAX_ITERATIONS, "npg not stable after %d iteration", i)) return;
  float convergedOn = npg.getStrongestValue();
  TS_assert(EQe(convergedOn, target, eps),
	    "converged to %f, expecting %f", convergedOn, target);
}

void testTrainWithConstant(){
  float sample = 10;
  for(int i=0; i<MAX_ITERATIONS; i++) {
    inSamples[i] = sample;
  }
  trainNpgAndCheckStability(sample, 0.0001);
}

void testTrainWithGaussianNoise(){
  float target = 100.f, sigma=4;
  for(int i=0; i<MAX_ITERATIONS; i++) {
   inSamples[i] = target + ((i%7)-3)*sigma/3.0;
  }
  trainNpgAndCheckStability(target, 0.5);
}

void testTrainWithSpreadNoise(){
  float target = 100.f;
  for(int i=0; i<MAX_ITERATIONS; i++) {
    inSamples[i] = (i%10<7)?target:1.0*(i%3 + 1)*(i%7 + 1)*(i%11 + 1);
  }
  trainNpgAndCheckStability(target, 0.01);
}

void testTrainWithNoiseFromOtherGaussian(){
  float target = 100.f, target2=50.f, sigma2=2;
  for(int i=0; i<MAX_ITERATIONS; i++) {
    inSamples[i] = (i%10<7)?target:target2 + ((i%7)-3)*sigma2/3.0;
  }
  trainNpgAndCheckStability(target, 0.01);
}
void testTrainWithChange(){
  float target = 100.f, target0=50.f;
  for(int i=0; i<MAX_ITERATIONS; i++) {
    inSamples[i] = (i<50)?target0:target;
  }
  trainNpgAndCheckStability(target, 0.01);
}

int main() {
  TS_addTest(testTrainWithConstant, "testTrainWithConstant");
  TS_addTest(testTrainWithGaussianNoise, "testTrainWithGaussianNoise");
  TS_addTest(testTrainWithSpreadNoise, "testTrainWithSpreadNoise");
  TS_addTest(testTrainWithNoiseFromOtherGaussian, "testTrainWithNoiseFromOtherGaussian");
  TS_addTest(testTrainWithChange, "testTrainWithChange");
  return TS_runTests("npg");
}
