
#include "npgbackground.h"
#include "testlib.h"
#include "bmp.h"

using namespace std;

#define MAX_ITERATIONS 1000
#define CONVERGENCE_STABILITY_IT 5

void testConstMatrix(){
  int w=2, h=1;
  Matrix<unsigned char> img(w,h);
  NPGBackground<float, 3> bg(w,h);

  for(int r=0; r<h; r++) {
    for(int c=0; c<w; c++) {
      MATRIX_EL(&img, r, c) = 11*r + 9*c;
    }
  }

  TS_assert(bg.fractionOfStable()<0.1, "expected to be instable before training");
  
  int i=0, stableIt=0;
  for (i=0; i<MAX_ITERATIONS && stableIt<CONVERGENCE_STABILITY_IT; i++) {
    bg.train(img);
    stableIt = (bg.fractionOfStable()>=0.99)?stableIt+1:0;
  }

  TS_debug("training completed in %d iterations", i);
  TS_assert(bg.fractionOfStable()>0.99, "expected to be all stable, but less than 90%");

  Matrix< NPG<float, 3> >* bgm = bg.getMatrix();
  
  for(int r=0; r<h; r++) {
    for(int c=0; c<w; c++) {
      float bgpix = MATRIX_EL(bgm, r, c).getStrongestValue();
      float pix = (float)MATRIX_EL(&img, r, c);
      TS_assert(EQe(bgpix, pix, 1), "wrong background pixel");
    }
  }
}


void testConstImg(){
  BmpImg<unsigned char> bmp;
  bmp.load("data/grey.bmp");
  int w=bmp.getW(), h=bmp.getH();
  Matrix<unsigned char> &img = *(bmp.getPixels());
  
  NPGBackground<float, 3> bg(w,h);

  TS_assert(bg.fractionOfStable()<0.1, "expected to be instable before training");

  int i=0, stableIt=0;
  for (i=0; i<MAX_ITERATIONS && stableIt<CONVERGENCE_STABILITY_IT; i++) {
    bg.train(img);
    stableIt = (bg.fractionOfStable()>=0.99)?stableIt+1:0;
  }

  TS_debug("training completed in %d iterations", i);
  TS_assert(bg.fractionOfStable()>0.99, "expected to be all stable, but less than 90%");

  Matrix< NPG<float, 3> >* bgm = bg.getMatrix();
  
  for(int r=0; r<h; r++) {
    for(int c=0; c<w; c++) {
      float bgpix = MATRIX_EL(bgm, r, c).getStrongestValue();
      float pix = (float)MATRIX_EL(&img, r, c);
      TS_assert(EQe(bgpix, pix, 1), "wrong background pixel");
    }
  }
}



int main() {
  TS_addTest(testConstMatrix, "testConstMatrix");
  TS_addTest(testConstImg, "testConstImg");
  //TS_addTest(, "");
  return TS_runTests("npg-background");
}
