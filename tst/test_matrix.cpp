#include<iostream>
#include<ctime>
#include<fstream>
#include<stdexcept>
#include<cmath>
#include<cstdio>

#include "matrix.h"
using namespace std;

#include "testlib.h"

void testCharMatrix() {
  int w=4, h=3;
  char i=0;
  Matrix<char> m(4,3,4);
  for(int r=0; r<h; r++){
    for(int c=0; c<w; c++){
      MATRIX_EL(&m, r, c) = 'a' + (i++);
    }
  }
  i=0;
  for(int r=0; r<h; r++){
    for(int c=0; c<w; c++){
      char expected = 'a' + (i++);
      TS_assert(MATRIX_EL(&m, r, c) == expected,
		"expected %c, found %c", expected, MATRIX_EL(&m, r, c));
    }
  }
}

void testVectorMatrix() {
  int w=4, h=3;
  int bg[4] = {255, 0, 100, 1};
  Matrix<int[4]> m(w,h,w);  
  //m.setAllElements(bg); DO NOT WORK WITH ARRAY (assignement)
  for(int r=0; r<h; r++){
    for(int c=0; c<w; c++){
      for(int j=0; j<4; j++) {
	MATRIX_EL(&m, r, c)[j] = (r==1 && c==2 && j==0)?33:bg[j];
      }
    }
  }
  m.el(1,2)[0] = 33;
  for(int r=0; r<h; r++){
    for(int c=0; c<w; c++){
      for(int j=0; j<4; j++) {
	int expected = (r==1 && c==2 && j==0)?33:bg[j];
	TS_assert(MATRIX_EL(&m, r, c)[j] == expected,
		"expected %c, found %c", expected, MATRIX_EL(&m, r, c));
      }
    }
  }
}

typedef struct {
  int sum;
  int v[4];
} AStruct;


void testStructMatrix() {
  int w=4, h=3;
  AStruct bg;
  bg.v[0] = 1;
  bg.v[1] = 2;
  bg.v[2] = 4;
  bg.v[3] = 8;
  bg.sum = 15;
  Matrix<AStruct> m(w,h,w);  
  m.setAllElements(bg);
  m.el(1,2).v[0] = 33;
  for(int r=0; r<h; r++){
    for(int c=0; c<w; c++){
      for(int j=0; j<4; j++) {
	int expected = (r==1 && c==2 && j==0)?33:bg.v[j];
	TS_assert(MATRIX_EL(&m, r, c).v[j] == expected,
		"expected %c, found %c", expected, MATRIX_EL(&m, r, c));
      }
    }
  }
}

void oneMatrixAllocationAndRelease(int w, int h){
  Matrix<double> m(w,h,w);
  for(int r=0; r<h; r++){
    for(int c=0; c<w; c++){
      MATRIX_EL(&m, r, c) = 10;
    }
  }
}

void testManyBigMatrixAllocationAndRelease(){
  for(int i=0; i<100; i++) {
    oneMatrixAllocationAndRelease(1000, 1000);
  }
}

void testFailOutOfBoundRow() {
  Matrix<double> m(1,2,1);
  m.el(2,0) = 0.3;
}

void testFailOutOfBoundCol() {
  Matrix<double> m(1,2);
  m.el(0,1) = 0.3;
}

void testFailOutOfBoundBoth() {
  Matrix<double> m(1,2);
  m.el(2,1) = 0.3;
}


void testFailWgtRowSize() {
  Matrix<double> m(5,8,4);
}

void testFailNullBuf() {
  Matrix<double> m(0, 5,8,4);
}

void testMatrixCopy() {
  if(1){
    Matrix<double> m(5,8);
    if(1){
      Matrix<double> m1 = m;
    }
  } //here without = overload, the test would fail
  if(1){
    Matrix<double> m(5,8);
    if(1){
      Matrix<double> m1(m);
    }
  } //here without copy constructor, the test would fail
}

void testMatrixAsWrapper() {
  double *buf = new double[100];
  if(1){
    Matrix<double> m(buf,9,10);
    for(int r=0; r<10; r++){
      for(int c=0; c<9; c++){
	MATRIX_EL(&m, r, c) = 10;
      }
    }
  }
  delete[] buf; //checking that Matrix did not delete the buffer already
}

int main() {
  TS_addTest(testCharMatrix, "testCharMatrix");
  TS_addTest(testVectorMatrix, "testVectorMatrix");
  TS_addTest(testStructMatrix, "testStructMatrix");
  TS_addTest(testManyBigMatrixAllocationAndRelease, "big allocation and release");

  TS_addTest(testMatrixCopy, "testMatrixCopy");
  TS_addTest(testMatrixAsWrapper, "testMatrixAsWrapper");
  
  TS_addTest(testFailOutOfBoundRow, "testFailOutOfBoundRow", 1);
  TS_addTest(testFailOutOfBoundCol, "testFailOutOfBoundCol", 1);
  TS_addTest(testFailOutOfBoundBoth, "testFailOutOfBoundBoth", 1);
  TS_addTest(testFailWgtRowSize, "testFailWgtRowSize", 1);
  TS_addTest(testFailNullBuf, "testFailNullBuf", 1);

  
  //TS_addTest(, "");
  
  return TS_runTests("matrix");
}
