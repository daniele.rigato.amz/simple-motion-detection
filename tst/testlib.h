#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>

#define TS_MAX_TESTS 500

typedef struct {
  void (*fun)();
  const char *name;
  int nerrors;
  int hasToBreak;
} TS_Test;

TS_Test TS_TESTS[TS_MAX_TESTS];
int TS_TESTS_N = 0;
int TS_CURRENT_TEST = 0;


#define TS_BT_BUF_SIZE 1000

void TS_printStackTrace(void) {
 int j, nptrs;
 void *buffer[TS_BT_BUF_SIZE];
 char **strings;
 nptrs = backtrace(buffer, TS_BT_BUF_SIZE);
 printf("backtrace() returned %d addresses\n", nptrs);
 /* The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)
    would produce similar output to the following: */
 strings = backtrace_symbols(buffer, nptrs);
 if (strings == NULL) {
   perror("backtrace_symbols");
 }
 for (j = 0; j < nptrs; j++)
   printf("%s\n", strings[j]);

 free(strings);
}


void TS_recordError(const char *errMsg) {
  TS_TESTS[TS_CURRENT_TEST].nerrors++;
}




/*** PUBLIC ***/

#define EQe(x,y,e) (((x>y)?(x-y):(y-x)) <= e)

void TS_debug(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  printf("[DEBUG]");
  vprintf(msg, args);
  printf("\n");
  va_end(args);
}

void TS_warning(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  printf("[WARNING]");
  vprintf(msg, args);
  printf("\n");
  va_end(args);
}

void TS_error(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  printf("[ERROR]");
  vprintf(msg, args);
  printf("\n");
  TS_recordError(msg);
  va_end(args);
}

int TS_assert(int value, const char *errMsg, ...) {
  if(!value) {    
    TS_recordError(errMsg);
    va_list args;
    va_start(args, errMsg);
    printf("[ERROR] assertion failed: ");
    vprintf(errMsg, args);
    printf("\n");
    va_end(args);
    TS_printStackTrace();
  }
  return !value;
}


 void TS_addTest(void (*fun)(), const char *name, int hasToBreak=0) {
  TS_TESTS[TS_TESTS_N].fun = fun;
  TS_TESTS[TS_TESTS_N].name = name;
  TS_TESTS[TS_TESTS_N].nerrors = 0;
  TS_TESTS[TS_TESTS_N].hasToBreak = hasToBreak;
  TS_TESTS_N++;
}

int TS_runTests(const char* suiteName) {
  printf("==== Testing suite %s ====\n", suiteName);
  int t;
  int nFailed = 0;
  for (t=0; t<TS_TESTS_N; t++) {
    TS_CURRENT_TEST = t;
    printf("- running test %s\n", TS_TESTS[t].name);

    try {
      TS_TESTS[t].fun();
      if(TS_TESTS[t].hasToBreak) {
	TS_recordError("[ERROR] test expected to thrown exception while it succeeded");
	printf("[ERROR] test expected to thrown exception while it succeeded");
      }
    } catch(std::exception const& e) {
      if(TS_TESTS[t].hasToBreak) {
	printf("exception thrown as expected\n");
      } else {
	TS_recordError("[ERROR] an unexpected exception happened");
	printf("[ERROR] an unexpected exception happened: %s\n", e.what());
      }
    } catch(...) {
      TS_recordError("[ERROR]Unknown exception occurred during the test");
      printf("[ERROR]Unknown exception occurred during the test\n");
    }
      
    if(TS_TESTS[t].nerrors==0) {
      printf("test %s succeeded\n", TS_TESTS[t].name);
    } else {
      printf("test %s failed with %d errors\n", TS_TESTS[t].name, TS_TESTS[t].nerrors);
      nFailed++;
    }
  }
  printf("= Test suite %s summary =\n", suiteName);
  printf("%d Failed, %d Succeeded, The suite %s %s\n", nFailed, TS_TESTS_N-nFailed, suiteName, (nFailed>0)?"failed":"succeeded");
  return nFailed;
}
