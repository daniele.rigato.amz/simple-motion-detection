/*
To collect the frames:

ffmpeg -i rtsp://192.168.220.1:554/onvif1 -r 1  -pix_fmt gray -vf format=gray,format=yuv422p /dev/shm/gframes/f%d.bmp
*/

#include<iostream>
#include<ctime>
#include<fstream>
#include <stdexcept>
#include <cmath>
#include <cstdio>

#include "bmp.h"
#include "npg.h"
#include "matrix.h"
#include "npgbackground.h"
#include "utils.h"

using namespace std;


void processFrames() {
  Crono crono;
  char framePath[100], elabPath[100];
  const char * rootDir = "/home/daniele/"; //"/dev/shm/"
  BmpImg<unsigned char> frameImg, stableImg;

  Matrix<float> filter(3,3);
  filter.setAllElements(1);

  Rectangle blobs[10];
  int nblobs=0;
  
  int startFrame = 5;
  snprintf(framePath, sizeof(framePath), "%s/gframes/f%d.bmp", rootDir, startFrame);
  frameImg.load(framePath);
  //loading the frame just because new image is not implemented yet
  stableImg.load(framePath); 
  NPGBackground<float, 3> bg(frameImg.getW(), frameImg.getH());
  Matrix<unsigned char> convBuf1(frameImg.getW(), frameImg.getH()),
    convBuf2(frameImg.getW(), frameImg.getH());
  for(int frame = startFrame; frame<130; frame++){
    cout << "processing frame " << frame << " last frame completed in" << crono;
    snprintf(framePath, sizeof(framePath), "%s/gframes/f%d.bmp",rootDir, frame);
    frameImg.load(framePath);

    bg.train(*frameImg.getPixels(), &convBuf1);
    bg.outputStable(*(stableImg.getPixels()), (unsigned char)0, (unsigned char)255);
    convBuf1.blobHilight(10,20,1);
    nblobs = convBuf1.findBlobs(blobs, 10);
    
    //frameImg.getPixels()->convolute(filter, convBuf1);
    //convBuf1.convolute(filter, convBuf2);
    //convBuf2.threshold(60);
    //convBuf2.amplify(255);
    frameImg.getPixels()->applyMask(convBuf1, (unsigned char)1, (unsigned char)0);
    int alarmDetected = 0;
    for(int i=0; i<nblobs; i++) {
      cout << "found blob:" << blobs[i] << "\n";
      if(blobs[i].r1 - blobs[i].r0 > 60 && blobs[i].c1 - blobs[i].c0 > 50 ){
	frameImg.getPixels()->drawRect(blobs[i], 255);
	alarmDetected = 1;
      }
    }
    snprintf(elabPath, sizeof(framePath), "%s/frameselab/e%d.bmp", rootDir, frame);
    if(alarmDetected){
      snprintf(elabPath, sizeof(framePath), "%s/frameselab/e%d-ALARM.bmp", rootDir, frame);
    }
    
    
    frameImg.save(elabPath);
    snprintf(elabPath, sizeof(framePath), "%s/frameselab/s%d.bmp", rootDir, frame);
    stableImg.save(elabPath);
  }
}


int main() {
  Crono crono;
  cout << "initialized in " << crono;
  processFrames();
  cout << "completed in " << crono;  
  return 0;
}
