#ifndef INCLUDE_NPG
#define INCLUDE_NPG

#include<iostream>
#include <stdexcept>
#include <cmath>

using namespace std;


typedef struct {
  double sigma;
  double alpha;
  double beta;
  double gamma;
  double stabilityFactor;
} NPGTrainingConfig;


/*
Naive Particle Gaussian
 */
template<class T, int N> class NPG {
private:
  T m[N]; //mean of the gaussians
  T w[N]; //weight
  int strongest;
public:
  NPG();
  void init();
  double train(T sample, NPGTrainingConfig &c);
  void print();
  
  int isStable(NPGTrainingConfig &c) {
    int stable = w[strongest] > c.stabilityFactor * 1.0 / (1.0 - c.alpha - c.beta);
    //   cout << "stable? w[" << strongest << "]=" << w[strongest]
    //	 << " > " <<  0.9 * 1.0 / (1.0 - c.alpha - c.beta) << "?" << stable <<"\n";
    return stable;
  }

  T getStrongestValue(){
    return m[strongest];
  }

  T getStrongestW(){
    return w[strongest];
  }
};


/*implementations of template class functions*/

template<class T, int N> NPG<T, N>::NPG() {
  init();
}

template<class T, int N> void NPG<T, N>::init() {
  for(int i=0; i<N; i++) {
    m[i] = 0;
    w[i] = 0;
  }
}

template<class T, int N> double NPG<T, N>::train(T sample, NPGTrainingConfig &c) {
  int weak = -1, strongIn = -1;
  int strongest = 0; int belongW = 1;
  for(int i=0; i<N; i++) {
    w[i] *= c.alpha;
    strongest = (w[i]>w[strongest])?i:strongest;
    if (weak<0 || w[i] <= w[weak]) {
      weak = i;
    }
    if ((strongIn<0 || w[i]>w[strongIn]) && abs(sample - m[i])<c.sigma) {
      strongIn = i;
    }
  }
  if (strongIn >= 0) {
    if(w[strongIn] >= 0.9) {
      m[strongIn] = c.gamma * sample + (1.0d - c.gamma) * m[strongIn];
      w[strongIn] = 1 + (c.alpha + c.beta) * w[strongIn] / c.alpha;
      strongest = (w[strongIn]>w[strongest])?strongIn:strongest;
      belongW = w[strongIn];
    } else {
      m[strongIn] = sample;
      w[strongIn] = 1;
    }
  } else {
    m[weak] = sample;
    w[weak] = 1;
  }
  this->strongest = strongest;
  return belongW;
}

template<class T, int N> void NPG<T, N>::print(){
  for (int i=0; i<N; i++){
    cout << m[i] << "[" << w[i] << "] ";
  }
  cout << "\n";
}


#endif //INCLUDE_NPG
