#ifndef INCLUDE_UTILS
#define INCLUDE_UTILS

#include<iostream>
#include<ctime>

using namespace std;

class Crono {
 private:
  clock_t prevt;
  clock_t curt;

 public:
  Crono();
  
  friend std::ostream& operator<< (std::ostream& stream, Crono crono);
};


/*Implementations*/

Crono::Crono() {
  prevt = clock();
}

std::ostream& operator<<(std::ostream& stream, Crono crono) {
  crono.curt = clock();
  stream << crono.curt - crono.prevt << " ticks, "
	 << ((float)(crono.curt-crono.prevt))/CLOCKS_PER_SEC << " s\n";
  crono.prevt = crono.curt;
}

#endif //INCLUDE_UTILS
