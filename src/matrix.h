#ifndef INCLUDE_MATRIX
#define INCLUDE_MATRIX

#include<iostream>

using namespace std;

  
class Rectangle{
 public:
  int r0;
  int r1;
  int c0;
  int c1;

  Rectangle() : r0(0), r1(0), c0(0), c1(0) {
  }

  Rectangle(int r0, int r1, int c0, int c1) : r0(r0), r1(r1), c0(c0), c1(c1) {
  }

  int intersect(Rectangle &r, int m) {
    return (
		(r0 < r.r0 + m && r.r0 < r1 + m && c0 < r.c0 + m && r.c0 < c1 + m) ||
		(r0 < r.r0 + m && r.r0 < r1 + m && c0 < r.c1 + m && r.c1 < c1 + m) ||
		(r0 < r.r1 + m && r.r1 < r1 + m && c0 < r.c0 + m && r.c0 < c1 + m) ||
		(r0 < r.r1 + m && r.r1 < r1 + m && c0 < r.c1 + m && r.c1 < c1 + m) ||

		(r.r0 < r0 + m && r0 < r.r1 + m && r.c0 < c0 + m && c0 < r.c1 + m) ||
		(r.r0 < r0 + m && r0 < r.r1 + m && r.c0 < c1 + m && c1 < r.c1 + m) ||
		(r.r0 < r1 + m && r1 < r.r1 + m && r.c0 < c0 + m && c0 < r.c1 + m) ||
		(r.r0 < r1 + m && r1 < r.r1 + m && r.c0 < c1 + m && c1 < r.c1 + m)
	   );
  }

  int merge(Rectangle &r, int m) {
    if(intersect(r, m)) {
      r0 = (r0<r.r0)?r0:r.r0;
      c0 = (c0<r.c0)?c0:r.c0;
      r1 = (r1>r.r1)?r1:r.r1;
      c1 = (c1>r.c1)?c1:r.c1;
      return 1;
    }
    return 0;
  }

  friend std::ostream& operator<< (std::ostream& stream, Rectangle r){
    cout << "rect{"<<r.r0<<" - "<<r.r1<<"|"<<r.c0<<" - "<<r.c1<<"}\n";
  }
  
};

template<class T>
class Matrix {
 private:
  T *dynamicBuffer;
  T *pdata;
 public:
  const unsigned int w;
  const unsigned int h;
  const unsigned int rowSize; //in case of padding rowSize>w, otherwise rowSize=w
  T *data;
  
  Matrix();
  Matrix(unsigned int w, unsigned int h);
  Matrix(T* data, unsigned int w, unsigned int h);
  Matrix(unsigned int w, unsigned int h, unsigned int rowSize);
  Matrix(T* data, unsigned int w, unsigned int h, unsigned int rowSize);
  Matrix(const Matrix<T> &copy);
  Matrix<T>& operator= (const Matrix<T> &copy);
  ~Matrix();
  
  T& el(unsigned int r, unsigned int c);

  inline int getW() {return w;}
  inline int getH() {return h;}

  void setAllElements(T value);
  void amplify(T value);
  void threshold(T value);
  template<class D> void applyMask(Matrix<D> &mask, D threshold, T bgPixel);
  template<class D> void applyInverseMask(Matrix<D> &mask, D threshold, T bgPixel);
  template<class D> void convolute(Matrix<D> &filter, Matrix<T> &output);
  void describe();
  void blobHilight(int minW, int minH, int fill);
  int findBlobs(Rectangle *outBlobs, int maxBlobs);
  void drawRect(Rectangle &r, T color);
  template <class T2> friend std::ostream& operator<< (std::ostream& stream, Matrix<T2> m);
};


#ifdef COMPILE_FOR_SPEED
#define MATRIX_EL(mp, r, c) (mp)->data[(r)*((mp)->rowSize) + (c)] 
#else
#define MATRIX_EL(mp, r, c) (mp)->el((r), (c))
#endif


/*Implementations*/


template<class T> Matrix<T>::~Matrix() {
  if(dynamicBuffer){
    delete[] dynamicBuffer;
    this->pdata = this->data = this->dynamicBuffer = 0;
  }
}


template<class T>
Matrix<T>::Matrix() : w(0), h(0), rowSize(0){
  this->pdata = this->data = this->dynamicBuffer = 0;
}


template<class T> Matrix<T>::Matrix(unsigned int w, unsigned int h, unsigned int rowSize) :
  w(w), h(h), rowSize(rowSize) {

  if(rowSize<w) {
    throw std::runtime_error("rowSize must be greater than w");
  }
  if(this->rowSize>0 && this->h>0) {
    this->pdata = this->data = this->dynamicBuffer = new T[rowSize * h];
  }
}


template<class T> Matrix<T>::Matrix(unsigned int w, unsigned int h) :
  w(w), h(h), rowSize(w) {

  if(rowSize<w) {
    throw std::runtime_error("rowSize must be greater than w");
  }
  if(this->rowSize>0 && this->h>0) {
    this->pdata = this->data = this->dynamicBuffer = new T[rowSize * h];
  }
}

template<class T>
Matrix<T>::Matrix(T* data, unsigned int w, unsigned int h, unsigned int rowSize) :
  w(w), h(h), rowSize(rowSize) {
  this->pdata = this->data = data;
  this->dynamicBuffer = 0;
  if(rowSize<w) {
    throw std::runtime_error("rowSize must be greater than w");
  }
  if(this->rowSize>0 && this->h>0 && data==0) {
    throw std::runtime_error("Non empty matrix must have a valid data vector, not null");
  }
}

template<class T> Matrix<T>::Matrix(T* data, unsigned int w, unsigned int h) :
w(w), h(h), rowSize(w) {
  this->pdata = this->data = data;
  this->dynamicBuffer = 0;
  if(rowSize<w) {
    throw std::runtime_error("rowSize must be greater than w");
  }
  if(this->rowSize>0 && this->h>0 && data==0) {
    throw std::runtime_error("Non empty matrix must have a valid data vector, not null");
  }
}

template<class T> Matrix<T>::Matrix(const Matrix<T> &copy) :
w(copy.w), h(copy.h), rowSize(copy.rowSize){
  this->pdata = this->data = copy.data;
  this->dynamicBuffer = 0;
}

template<class T> Matrix<T>& Matrix<T>::operator= (const Matrix<T> &copy) {
  this->w = copy.w;
  this->h = copy.h;
  this->rowSize = copy.rowSize;
  this->pdata = this->data = copy.data;
  this->dynamicBuffer = 0;
  return *this;
}

template<class T> inline T& Matrix<T>::el(unsigned int r, unsigned int c){
  if(r<0 || r>=h || c<0 || c>=w || r*rowSize + c > rowSize * h || pdata==0){
    cerr << "Matrix out of bound (" << r << ", " << c
	 << ") out of (" << h << ", " << w << ")\n";
    throw std::runtime_error("Matrix OUT OF BOUND");
  }
  return pdata[r*rowSize + c];
}

//TODO optimize
template<class T> void Matrix<T>::drawRect(Rectangle &re, T color){
  for (int r=re.r0; r<=re.r1; r++) {
    MATRIX_EL(this, r, re.c0) = color;
    MATRIX_EL(this, r, re.c0+1) = color;
    MATRIX_EL(this, r, re.c1-1) = color;
    MATRIX_EL(this, r, re.c1) = color;
  }
  for (int c=re.c0; c<=re.c1; c++) {
    MATRIX_EL(this, re.r0, c) = color;
    MATRIX_EL(this, re.r0+1, c) = color;
    MATRIX_EL(this, re.r1-1, c) = color;
    MATRIX_EL(this, re.r1, c) = color;
  }
}


template<class T> void Matrix<T>::setAllElements(T value){
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w; c++) {
      MATRIX_EL(this, r, c) = value;
    }
  }
}

template<class T> void Matrix<T>::amplify(T value){
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w; c++) {
      MATRIX_EL(this, r, c) *= value;
    }
  }
}

template<class T> void Matrix<T>::threshold(T value){
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w; c++) {
      if(MATRIX_EL(this, r, c) < value) {
	MATRIX_EL(this, r, c) = 0;
      }
    }
  }
}


template<class T> void Matrix<T>::blobHilight(int minW, int minH, int fill){
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w-fill-1; c++) {
      for (unsigned int j=1; j<=fill+1; j++) {
	if(MATRIX_EL(this, r, c+j)>0) {
	  MATRIX_EL(this, r, c+j) += MATRIX_EL(this, r, c);
	}
      }
    }
    for (unsigned int c=w-1; c>fill; c--) {
      for (unsigned int j=1; j<=fill+1; j++) {
	if(MATRIX_EL(this, r, c-j)>0 && MATRIX_EL(this, r, c) > MATRIX_EL(this, r, c-j)) {
	  MATRIX_EL(this, r, c-j) = MATRIX_EL(this, r, c);
	}
      }
    }
    for (unsigned int c=0; c<w; c++) {
      MATRIX_EL(this, r, c) = (MATRIX_EL(this, r, c)<minW)?0:1;
    }
  }
  
  for (unsigned int r=0; r<h-fill-1; r++) {
    for (unsigned int c=0; c<w; c++) {
      for (unsigned int j=1; j<=fill+1; j++) {
	if(MATRIX_EL(this, r+j, c)>0) {
	  MATRIX_EL(this, r+j, c) += MATRIX_EL(this, r, c);
	}
      }
    }
  }
  for (unsigned int r=h-1; r>fill; r--) {
    for (unsigned int c=0; c<w; c++) {
      for (unsigned int j=1; j<=fill+1; j++) {
	if(MATRIX_EL(this, r-j, c)>0 && MATRIX_EL(this, r, c) > MATRIX_EL(this, r-j, c)) {
	  MATRIX_EL(this, r-j, c) = MATRIX_EL(this, r, c);
	}
      }
    }
  }
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w; c++) {
	MATRIX_EL(this, r, c) = (MATRIX_EL(this, r, c)<minH)?0:1;
    }
  }
}

template<class T>
int Matrix<T>::findBlobs(Rectangle *outBlobs, int maxBlobs) {
  int nBlobs = 0;
  for (unsigned int r=0; r<h; r++) {
    Rectangle seg(r,r,-1,-1);
    for (unsigned int c=0; c<w; c++) {
      if(seg.c0==-1) {
	//looking for segment start
	if(MATRIX_EL(this, r, c)>0){
	  seg.c0 = c;
	  seg.c1 = c;
	}
      }else{
	if(MATRIX_EL(this, r, c)>0){
	  //riding the segment
	  seg.c1 = c;
	}
	//is the segment over?
	if(c==w-1 || seg.c1 < c - 20) {
	  //the segment is over, looking for rectangles to update
	  int updatedRectangles = 0;
	  for(int j=0; j<nBlobs; j++) {
	    updatedRectangles += outBlobs[j].merge(seg, 20); //TODO magic 2
	  }
	  if(updatedRectangles==0 && nBlobs<maxBlobs) {
	    outBlobs[nBlobs] = seg;
	    nBlobs++;
	  }
	  seg = Rectangle(r,r,-1,-1);
	}
      }
    }
  }
  //squash blobs
  
  int squashed;
  do {
    squashed = 0;
    int last=0;
    for(int i=0; i<nBlobs; i++){
      int thisSquashed = 0;
      for(int j=last-1; j>=0; j--){
	//cout << "merging "<<i<<" on "<<j<<"\n";
	thisSquashed = outBlobs[j].merge(outBlobs[i], 20); //TODO magic
	squashed += thisSquashed;
      }
      if(thisSquashed==0){
	//cout <<i<<" unmergeable, saving on "<<last<<"\n";
	outBlobs[last] =outBlobs[i];
	last++;
      }
    }
    if(last<nBlobs){
      nBlobs = last;
    }else{
      break;
    }
  }while(squashed>0);
  
  return nBlobs;
}

template<class T> template<class D>
void Matrix<T>::applyMask(Matrix<D> &mask, D threshold, T bgPixel){
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w; c++) {
      if(MATRIX_EL(&mask, r, c) < threshold) {
	MATRIX_EL(this, r, c) = bgPixel;
      }
    }
  }
}


template<class T> template<class D>
void Matrix<T>::applyInverseMask(Matrix<D> &mask, D threshold, T bgPixel){
  for (unsigned int r=0; r<h; r++) {
    for (unsigned int c=0; c<w; c++) {
      if(MATRIX_EL(&mask, r, c) >= threshold) {
	MATRIX_EL(this, r, c) = bgPixel;
      }
    }
  }
}

template<class T> template<class D>
void Matrix<T>::convolute(Matrix<D> &f, Matrix<T> &o){
  for (unsigned int r=0; r<h-f.h; r++) {
    for (unsigned int c=0; c<w-f.w; c++) {
      D convResult = 0;
      for (unsigned int fr=0; fr<f.h; fr++) {
	for (unsigned int fc=0; fc<f.w; fc++) {
	  convResult += ((D)MATRIX_EL(this, r+fr, c+fc))*(MATRIX_EL(&f, fr, fc));
	}
      }
      MATRIX_EL(&o, r+f.h/2, c+f.w/2) = ((T)convResult);
    }
  }
}

template<class T> void Matrix<T>::describe(){
  cout << "d:" << (long)data << " w:" << w << " h:" << h
       << " rowSize:" << rowSize << "\n";
}

template<class T> std::ostream& operator<<(std::ostream& stream, Matrix<T> m) {
  for (unsigned int r=0; r<m.h; r++) {
    stream << "\n";
    for (unsigned int c=0; c<m.w; c++) {
      stream << "\t|" << MATRIX_EL(&m, r, c) << " ";
    }
  }
  stream << "\n";
}


#endif //INCLUDE_MATRIX
