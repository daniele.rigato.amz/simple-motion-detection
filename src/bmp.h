#ifndef INCLUDE_BMP
#define INCLUDE_BMP

#include<iostream>
#include<ctime>
#include<fstream>
#include <stdexcept>


#include "matrix.h"
template<class Pixel>
class BmpImg {
 private:
  char *dynamicBuffer;
  int dynamicBufferSize;
  char headerType[3];
  int filesize;
  int offset;
  int w;
  int rsize; //bytes
  int h;
  int pixelSize; //bits
  int compressionType;
  Matrix<Pixel> *pixels;
  
  void redimDynamicBuffer(int len) {
    char *oldBuf = dynamicBuffer;
    dynamicBufferSize = len;
    dynamicBuffer = new char[dynamicBufferSize];
    if(oldBuf!=0){
      delete[] oldBuf;
    }
  }
  
 public:
  BmpImg(){
      dynamicBufferSize = 100;
      dynamicBuffer = new char[dynamicBufferSize];
      pixels = 0;
  }
  ~BmpImg(){
    if(dynamicBuffer!=0){
      delete[] dynamicBuffer;
      dynamicBuffer = 0;
    }
    if(pixels) {
      delete pixels;
    }
  }
  
  void load(const char *filepath);
  void save(const char *filepath);
  void describe();

  inline Matrix<Pixel> *getPixels(){return pixels;}
  inline int getW() {return w;}
  inline int getH() {return h;}
};


template<class Pixel> void BmpImg<Pixel>::load(const char *filepath) {
  std::ifstream fin(filepath, ios::in | ios::binary );
  fin.read(dynamicBuffer, dynamicBufferSize);
  fin.close();
  headerType[0] = dynamicBuffer[0];
  headerType[1] = dynamicBuffer[1];
  headerType[2] = '\0';
  if(headerType[0]!='B' || headerType[1]!='M'){
    throw std::runtime_error("Bitmap header not supported");
  }
  filesize = *((int*)(dynamicBuffer+2));
  if(filesize > dynamicBufferSize) {
    redimDynamicBuffer(filesize);
    std::ifstream fin2(filepath, ios::in | ios::binary );
    fin2.read(dynamicBuffer, dynamicBufferSize);
    fin2.close();
  }
  offset = *((int*)(dynamicBuffer+10));
  w = (int)(*((int*)(dynamicBuffer+18)));
  h = (int)(*((int*)(dynamicBuffer+22)));
  pixelSize = (int)(*((unsigned short*)(dynamicBuffer+28)));
  if(pixelSize != 8) {
    throw std::runtime_error("Only grey-scale 8-bit bitmap are supported");
  }
  compressionType = (int)(*((int*)(dynamicBuffer+30)));
  if(compressionType != 0) {
    throw std::runtime_error("Compressed bitmap not supported");
  }
  rsize = ((pixelSize * w + 31) / 32) * 4;

  if(pixels) {
    delete pixels;
  }
  pixels = new Matrix<Pixel>((Pixel*)(dynamicBuffer + offset), w, h, rsize);
}

template<class Pixel> void BmpImg<Pixel>::save(const char *filepath) {
  std::ofstream fout(filepath, ios::binary);
  fout.write(dynamicBuffer, filesize);
  fout.close();
}

template<class Pixel> void BmpImg<Pixel>::describe(){
  cout << "BmpImg{header type:"<< headerType << " file size:" << filesize
       << " dim: " << w << "x" << h << " x " <<pixelSize << "}\n";
}


#endif //INCLUDE_BMP
