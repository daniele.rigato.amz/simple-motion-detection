#ifndef INCLUDE_NPGBACKGROUND
#define INCLUDE_NPGBACKGROUND

#include "matrix.h"
#include "npg.h"

template<class T, unsigned int NHIP>
class NPGBackground {

 private:
  NPGTrainingConfig trainingConfig;
  Matrix< NPG<T, NHIP> > m;
 public:

  
  NPGBackground(unsigned int w, unsigned int h) : m(w,h) {
    trainingConfig.sigma = 5;
    trainingConfig.alpha = 0.97;
    trainingConfig.beta = -0.13;
    trainingConfig.gamma = 0.1;
    trainingConfig.stabilityFactor = 0.7;
  }

  template<class Pixel> void train(Matrix<Pixel> &imgMat, Matrix<Pixel> *classMat=0) {
    for(int r=0; r<m.h; r++) {
      for(int c=0; c<m.w; c++) {
	NPG<T, NHIP> &npg = MATRIX_EL(&m, r, c);
	double wpix = npg.train(MATRIX_EL(&imgMat, r, c), trainingConfig);
	if(classMat) {
	  if(npg.isStable(trainingConfig) && wpix<2){ //TODO 2??
	    MATRIX_EL(classMat, r, c) = 1;
	  } else {
	    MATRIX_EL(classMat, r, c) = 0;
	  }
	}
      }
    }
  }

  float fractionOfStable(){
    float sum=0, n=0;
    for(int r=0; r<m.h; r++) {
      for(int c=0; c<m.w; c++) {
	NPG<T, NHIP> &npg = MATRIX_EL(&m, r, c);
	if(npg.isStable(trainingConfig)){
	  sum+=1.f;
	}
	n+=1.f;
      }
    }
    return sum / n;
  }

  inline Matrix< NPG<T, NHIP> >* getMatrix(){return &m;}

  template<class Pixel>
  void outputStable(Matrix<Pixel> &outMat, Pixel stable, Pixel unstable) {
    for(int r=0; r<m.h; r++) {
      for(int c=0; c<m.w; c++) {
	NPG<T, NHIP> &npg = MATRIX_EL(&m, r, c);
	Pixel p =(npg.isStable(trainingConfig))?stable:unstable;
	MATRIX_EL(&outMat, r, c) = p;
      }
    }
  }

};

#endif //INCLUDE_NPGBACKGROUND
