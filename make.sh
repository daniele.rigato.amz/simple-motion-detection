#!/usr/bin/env bash

PROGNAME='imgproc'

FAILURES=0
FAILURES_STR=""

DATE=`date +%Y/%m/%d/%H`

printf "\n\n\n============== MAKE $DATE =============\n\n"


echo "========= BACKUP ==========";

if [ ! -d "backup" ]; then
    mkdir backup
fi
mkdir -p "backup/${DATE}/" && \
cp -r src "backup/${DATE}/" && \
cp -r tst "backup/${DATE}/" && \
cp make.sh "backup/${DATE}/" || \
exit 1

echo "backup created: ./backup/${DATE}"


echo "========= TESTING ==========";

if [ -d "build" ]; then
 rm -rf build
fi
mkdir build
cp -r data build/
cp src/*.h build/
cp src/*.cpp build/

cp tst/*.h build/
for test in tst/*.cpp
do
 echo "Testing $test"
 cp $test build/current_test.cpp
 cd build
 g++ -g -rdynamic current_test.cpp -o current_test
 if [ $? -eq 0 ]
 then
     echo "$test compiled"
     ./current_test
     if [ $? -eq 0 ]
     then
         echo "$test succeeded"
     else
         echo "FAILURE: $test failed"
         FAILURES=$((FAILURES+1))
         FAILURES_STR="$FAILURES_STR\n - test $test failed"
     fi
 else
     echo "FAILURE: could not compile $test"
     FAILURES=$((FAILURES+1))
     FAILURES_STR="$FAILURES_STR\n -could not compile $test"
 fi
 rm current_test
 cd ../
 echo "Test $test completed"
 echo ""
done



echo "========= COMPILING ==========";

rm build/*.cpp
rm build/*.h
cp src/*.h build/
cp src/*.cpp build/

cd build
g++ -g -rdynamic *.cpp -o ../"${PROGNAME}-dbg"
if [ $? -eq 0 ]
then
    echo "${PROGNAME}-dbg compiled"
    g++ *.cpp -o ../$PROGNAME
    if [ $? -eq 0 ]
    then
        echo "$PROGNAME compiled"
    else
        echo "FAILURE: could not compile $PROGNAME"
        FAILURES=$((FAILURES+1))
        FAILURES_STR="$FAILURES_STR\n - could not compile $PROGNAME"
    fi
else
    echo "FAILURE: could not compile ${PROGNAME}dbg"
    FAILURES=$((FAILURES+1))
    FAILURES_STR="$FAILURES_STR\n - could not compile $PROGNAME.dbg"
fi
cd ..


printf "\n\n\n============== Result =============\n\n"
if [ $FAILURES -eq 0 ]
then
    echo "compilation succeeded with no failures"
else
    echo "compilation FAILED with $FAILURES failures:"
    printf "$FAILURES_STR\n\n"
    exit 1
fi
